package com.egnyte.utils.auditreporter;

public class AuditReporterException extends Exception {

    public AuditReporterException(String message) {
        super(message);
    }
}
