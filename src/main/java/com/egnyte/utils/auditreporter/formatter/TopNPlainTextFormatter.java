package com.egnyte.utils.auditreporter.formatter;

import com.egnyte.utils.auditreporter.AuditReporterData;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TopNPlainTextFormatter extends DefaultPlainTextFormatter {

    private static final String HEADER = "Top #3 Report\n============";

    @Override
    public String getHeader() {
        return HEADER;
    }

    @Override
    public String formatData(AuditReporterData data) {
        StringBuilder formattedData = new StringBuilder();
        data.getFiles().forEach(file -> formattedData.append(formatFile(file, getUserMapping(data))));
        return formattedData.toString();
    }

    private Map<String, String> getUserMapping(AuditReporterData command) {
        return command.getUsers().stream()
                    .collect(Collectors.toMap(
                            o -> o.get(USER_ID_INDEX),
                            o -> o.get(USER_NAME_INDEX)));
    }

    private String formatFile(List<String> fileData, Map<String, String> usersMapping) {
        return String.format("* %s ==> user %s, %s bytes\n",
                fileData.get(FILE_NAME_INDEX),
                usersMapping.getOrDefault(fileData.get(FILE_OWNER_USER_ID_INDEX), "n/a"),
                fileData.get(FILE_SIZE_INDEX));
    }
}
