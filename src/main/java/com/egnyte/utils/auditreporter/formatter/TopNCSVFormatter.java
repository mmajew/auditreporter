package com.egnyte.utils.auditreporter.formatter;

public class TopNCSVFormatter extends CSVFormatter {

    private static final String TOP_N_TEMPLATE = String.format("%s,%s,%s",
            FILE_NAME_PLACEHOLDER, USER_PLACEHOLDER, FILE_SIZE_PLACEHOLDER);
    private static final String HEADER = "Top #3 Report\n============";

    @Override
    public String getTemplate() {
        return TOP_N_TEMPLATE;
    }

    @Override
    public String getHeader() {
        return HEADER;
    }
}
