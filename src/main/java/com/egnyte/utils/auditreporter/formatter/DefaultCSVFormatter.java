package com.egnyte.utils.auditreporter.formatter;

public class DefaultCSVFormatter extends CSVFormatter {

    private static final String DEFAULT_TEMPLATE = String.format("%s,%s,%s",
            USER_PLACEHOLDER, FILE_NAME_PLACEHOLDER, FILE_SIZE_PLACEHOLDER);

    @Override
    public String getTemplate() {
        return DEFAULT_TEMPLATE;
    }
}
