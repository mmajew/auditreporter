package com.egnyte.utils.auditreporter.formatter;

import com.egnyte.utils.auditreporter.AuditReporterData;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class CSVFormatter extends OutputFormatter {
    private static final String TEMPLATE_PREFIX = "%(";
    private static final String TEMPLATE_SUFFIX = "%(";

    private static final String USER_KEY = "owner";
    private static final String FILE_NAME_KEY = "fileName";
    private static final String FILE_SIZE_KEY = "fileSize";

    static final String USER_PLACEHOLDER = TEMPLATE_PREFIX + USER_KEY + TEMPLATE_SUFFIX;
    static final String FILE_NAME_PLACEHOLDER = TEMPLATE_PREFIX + FILE_NAME_KEY + TEMPLATE_SUFFIX;
    static final String FILE_SIZE_PLACEHOLDER = TEMPLATE_PREFIX + FILE_SIZE_KEY + TEMPLATE_SUFFIX;

    private static final String HEADER = "Audit Report\n============";

    @Override
    public String getHeader() {
        return HEADER;
    }

    @Override
    public String formatData(AuditReporterData data) {
        Map<String, String> userNames = parseUsers(data.getUsers());
        return data.getFiles().stream()
                .map(file -> formatLine(userNames, file))
                .collect(Collectors.joining("\n"));
    }

    private String formatLine(Map<String, String> userNames, List<String> file) {
        Map<String, String> substitutionValues = new HashMap<>(3);
        substitutionValues.put(USER_KEY, userNames.getOrDefault(file.get(FILE_OWNER_USER_ID_INDEX), ""));
        substitutionValues.put(FILE_NAME_KEY, file.get(FILE_NAME_INDEX));
        substitutionValues.put(FILE_SIZE_KEY, file.get(FILE_SIZE_INDEX));
        StrSubstitutor substitutor = new StrSubstitutor(substitutionValues, TEMPLATE_PREFIX, TEMPLATE_SUFFIX);
        return substitutor.replace(getTemplate());
    }

    private Map<String, String> parseUsers(List<List<String>> users) {
        return users
                .stream()
                .collect(Collectors.toMap(
                        user -> user.get(USER_ID_INDEX),
                        user -> user.get(USER_NAME_INDEX)));
    }

    abstract String getTemplate();
}
