package com.egnyte.utils.auditreporter.formatter;

import com.egnyte.utils.auditreporter.AuditReporterData;

public abstract class OutputFormatter {

    int USER_ID_INDEX = 0;
    int USER_NAME_INDEX = 1;

    int FILE_SIZE_INDEX = 1;
    int FILE_NAME_INDEX = 2;
    int FILE_OWNER_USER_ID_INDEX = 3;

    public abstract String getHeader();

    public abstract String formatData(AuditReporterData data);


}
