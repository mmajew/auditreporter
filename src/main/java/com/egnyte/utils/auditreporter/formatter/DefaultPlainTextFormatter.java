package com.egnyte.utils.auditreporter.formatter;

import com.egnyte.utils.auditreporter.AuditReporterData;

import java.util.List;

public class DefaultPlainTextFormatter extends OutputFormatter {

    private static final String HEADER = "Audit Report\n============";

    @Override
    public String getHeader() {
        return HEADER;
    }

    @Override
    public String formatData(AuditReporterData data) {
        StringBuilder formattedData = new StringBuilder();
        for (List<String> userRow : data.getUsers()) {
            long userId = Long.parseLong(userRow.get(USER_ID_INDEX));
            formattedData.append(getUserHeader(userRow));
            formattedData.append(getOnlyOwnerFiles(userId, data.getFiles()));
        }
        return formattedData.toString();
    }

    private String getUserHeader(List<String> userRow) {
        String userName = userRow.get(USER_NAME_INDEX);
        return(String.format("## User: %s\n", userName));
    }

    private String getOnlyOwnerFiles(long ownerId, List<List<String>> files) {
        StringBuilder filesData = new StringBuilder();
        for (List<String> fileRow : files) {
            long size = Long.parseLong(fileRow.get(FILE_SIZE_INDEX));
            String fileName = fileRow.get(FILE_NAME_INDEX);
            long ownerUserId = Long.parseLong(fileRow.get(FILE_OWNER_USER_ID_INDEX));
            if (ownerUserId == ownerId) {
                filesData.append(formatFileData(fileName, size));
            }
        }
        return filesData.toString();
    }

    private String formatFileData(String fileName, long fileSize) {
        return(String.format("* %s ==> %s bytes\n", fileName, fileSize));
    }
}
