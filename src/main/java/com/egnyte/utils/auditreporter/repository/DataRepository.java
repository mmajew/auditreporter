package com.egnyte.utils.auditreporter.repository;

import com.egnyte.utils.auditreporter.AuditReporterException;

import java.util.List;

public interface DataRepository {

    List<List<String>> readData() throws AuditReporterException;

}
