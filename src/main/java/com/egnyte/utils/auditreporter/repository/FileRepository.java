package com.egnyte.utils.auditreporter.repository;

import com.egnyte.utils.auditreporter.AuditReporterException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileRepository implements DataRepository {

    private final String fileName;

    public FileRepository(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<List<String>> readData() throws AuditReporterException {
        List<List<String>> fileContent = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader.readLine(); // skip header
            String line;
            while ((line = reader.readLine()) != null) {
                fileContent.add(Arrays.asList(line.split(",")));
            }
        } catch (IOException exception) {
            throw new AuditReporterException(String.format("Can't open file %s", fileName));
        }
        return fileContent;
    }
}
