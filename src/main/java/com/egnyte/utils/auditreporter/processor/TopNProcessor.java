package com.egnyte.utils.auditreporter.processor;

import com.egnyte.utils.auditreporter.AuditReporterData;
import com.egnyte.utils.auditreporter.AuditReporterException;

import java.util.Comparator;
import java.util.stream.Collectors;

public class TopNProcessor implements Processor {
    private static int FILE_SIZE_INDEX = 1;

    private final int topNumber;

    public TopNProcessor(int topNumber) {
        if (topNumber < 0) {
            throw new IllegalArgumentException("Wrong top limit. Should be positive number");
        }
        this.topNumber = topNumber;

    }

    @Override
    public void processData(AuditReporterData auditReporterData) {
        if (auditReporterData != null && auditReporterData.getFiles() != null) {
            auditReporterData.getFiles().sort(Comparator.comparingLong(o -> -Long.parseLong(o.get(FILE_SIZE_INDEX))));
            auditReporterData.setFiles(
                    auditReporterData.getFiles().stream()
                            .limit(topNumber)
                            .collect(Collectors.toList()));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TopNProcessor that = (TopNProcessor) o;

        return topNumber == that.topNumber;
    }

    @Override
    public int hashCode() {
        return topNumber;
    }
}
