package com.egnyte.utils.auditreporter.processor;

import com.egnyte.utils.auditreporter.AuditReporterData;

public interface Processor {

    void processData(AuditReporterData data);
}
