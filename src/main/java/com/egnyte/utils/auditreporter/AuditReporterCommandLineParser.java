package com.egnyte.utils.auditreporter;

import com.egnyte.utils.auditreporter.formatter.*;
import com.egnyte.utils.auditreporter.processor.Processor;
import com.egnyte.utils.auditreporter.processor.TopNProcessor;
import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;

class AuditReporterCommandLineParser {

    private static final int USER_FILE_NAME_ARG_INDEX = 0;
    private static final int FILE_FILE_NAME_ARG_INDEX = 1;
    private static final String TOP_LONG_ARG = "top";
    private static final String TOP_ARG = "t";
    private static final String CSV_ARG = "c";

    private CommandLine commandLine;
    private Options options;

    AuditReporterCommandLineParser(){
        options = initOptions();
    }

    void parse(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
        if (commandLine.getArgs().length != 2) {
            throw new ParseException("[USERS FILE] [FILES FILE] required");
        }
    }

    private Options initOptions() {
        Options options = new Options();
        options.addOption(Option.builder(CSV_ARG)
                .hasArg(false)
                .desc("prints results in CSV format")
                .required(false)
                .build());
        options.addOption(Option.builder(TOP_ARG)
                .longOpt(TOP_LONG_ARG)
                .required(false)
                .desc("prints n-largest files sorted by size (n should be positive number)")
                .hasArg()
                .numberOfArgs(1)
                .type(Integer.class)
                .build());
        return options;
    }

    String getUsersFileName() {
        return commandLine.getArgList().get(USER_FILE_NAME_ARG_INDEX);
    }

    String getFilesFileName() {
        return commandLine.getArgList().get(FILE_FILE_NAME_ARG_INDEX);
    }

    OutputFormatter getPresenter() {
        OutputFormatter activeOutputFormatter;
        if (commandLine.hasOption(TOP_ARG)) {
            activeOutputFormatter = commandLine.hasOption(CSV_ARG) ? new TopNCSVFormatter() : new TopNPlainTextFormatter();
        } else {
            activeOutputFormatter = commandLine.hasOption(CSV_ARG) ? new DefaultCSVFormatter() : new DefaultPlainTextFormatter();
        }
        return activeOutputFormatter;
    }

    List<Processor> getProcessors() throws ParseException {
        List<Processor> activeProcessors = new ArrayList<>();
        if (commandLine.hasOption(TOP_ARG)) {
            try {
                activeProcessors.add(new TopNProcessor(Integer.parseInt(commandLine.getOptionValue("top"))));
            } catch (IllegalArgumentException e) {
                throw new ParseException(e.getMessage());
            }
        }
        return activeProcessors;
    }

    void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("java -jar auditreporter.jar [USERS FILE] [FILES FILE] ", options);
    }
}
