package com.egnyte.utils.auditreporter;

import com.egnyte.utils.auditreporter.formatter.OutputFormatter;
import com.egnyte.utils.auditreporter.processor.Processor;
import com.egnyte.utils.auditreporter.repository.DataRepository;
import com.egnyte.utils.auditreporter.repository.FileRepository;
import org.apache.commons.cli.ParseException;

import java.util.List;

public class Runner {

    private final String usersFileName;
    private final String filesFileName;
    private final OutputFormatter outputFormatter;
    private final List<Processor> processors;

    private Runner(String usersFileName, String filesFileName, OutputFormatter outputFormatter, List<Processor> processors) throws ParseException {
        this.usersFileName = usersFileName;
        this.filesFileName = filesFileName;
        this.outputFormatter = outputFormatter;
        this.processors = processors;
    }

    public static void main(String[] args) throws AuditReporterException {
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();

        try {
            commandLineParser.parse(args);
            Runner r = new Runner(commandLineParser.getUsersFileName(),
                    commandLineParser.getFilesFileName(),
                    commandLineParser.getPresenter(),
                    commandLineParser.getProcessors());
            AuditReporterData data = r.loadData();
            r.processData(data);
            r.run(data);
        } catch (ParseException parseException) {
            commandLineParser.printHelp();
        }
    }

    private void run(AuditReporterData data) {
        System.out.println(outputFormatter.getHeader());
        System.out.println(outputFormatter.formatData(data));
    }

    private AuditReporterData loadData() throws AuditReporterException {
        DataRepository userRepository = new FileRepository(usersFileName);
        DataRepository fileRepository = new FileRepository(filesFileName);
        return new AuditReporterData(userRepository.readData(), fileRepository.readData());
    }

    private void processData(AuditReporterData data) {
        processors.forEach(processor -> processor.processData(data));
    }
}
