package com.egnyte.utils.auditreporter;

import java.util.List;

public class AuditReporterData {
    private List<List<String>> users;
    private List<List<String>> files;

    public AuditReporterData(List<List<String>> users, List<List<String>> files) {
        this.users = users;
        this.files = files;
    }

    public List<List<String>> getUsers() {
        return users;
    }

    public List<List<String>> getFiles() {
        return files;
    }

    public void setFiles(List<List<String>> files) {
        this.files = files;
    }
}
