package com.egnyte.utils.auditreporter.processor;

import com.egnyte.utils.auditreporter.AuditReporterData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.egnyte.utils.auditreporter.TestHelper.getFile;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class TopNProcessorTest {

    private static final int TOP_N = 3;

    private TopNProcessor processor;

    @Before
    public void setUp() throws Exception {
        processor = new TopNProcessor(TOP_N);
    }

    @Test
    public void top3WithMoreThen3FilesTest() throws Exception {
        // Given
        List<List<String>> files = new ArrayList<>();
        files.add(getFile("FILE1", 2000L, "NAME1", "1"));
        files.add(getFile("FILE2", 6000L, "NAME2", "2"));
        files.add(getFile("FILE3", 1000L, "NAME3", "3"));
        files.add(getFile("FILE4", 900L, "NAME4", "4"));
        List<List<String>> users = new ArrayList<>();
        AuditReporterData reporterData = new AuditReporterData(users, files);
        // When
        processor.processData(reporterData);
        // Then
        assertThat(reporterData.getFiles())
                .hasSize(3)
                .containsExactly(
                        getFile("FILE2", 6000L, "NAME2", "2"),
                        getFile("FILE1", 2000L, "NAME1", "1"),
                        getFile("FILE3", 1000L, "NAME3", "3"));
        assertThat(reporterData.getUsers()).isEqualTo(users);
    }

    @Test
    public void top3WithLessThen3FilesTest() throws Exception {
        // Given
        List<List<String>> files = new ArrayList<>();
        files.add(getFile("FILE1", 2000L, "NAME1", "1"));
        files.add(getFile("FILE2", 6000L, "NAME2", "2"));
        List<List<String>> users = new ArrayList<>();
        AuditReporterData reporterData = new AuditReporterData(users, files);
        // When
        processor.processData(reporterData);
        // Then
        assertThat(reporterData.getFiles())
                .hasSize(2)
                .containsExactly(
                        getFile("FILE2", 6000L, "NAME2", "2"),
                        getFile("FILE1", 2000L, "NAME1", "1"));
        assertThat(reporterData.getUsers()).isEqualTo(users);
    }

    @Test
    public void top3WithNullFilesTest() throws Exception {
        // Given
        AuditReporterData reporterData = new AuditReporterData(null, null);
        // When
        processor.processData(reporterData);
        // Then
        assertThat(reporterData.getFiles()).isNull();
        assertThat(reporterData.getUsers()).isNull();
    }

    @Test
    public void top3WithNullDataTest() throws Exception {
        // Given
        AuditReporterData reporterData = null;
        // When
        processor.processData(reporterData);
        // Then
        assertThat(reporterData).isNull();
    }

    @Test
    public void topNegativeTest() throws Exception {
        // Given
        // When
        Throwable throwable = catchThrowable(() -> new TopNProcessor(-3));
        // Then
        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Wrong top limit. Should be positive number");
    }
}