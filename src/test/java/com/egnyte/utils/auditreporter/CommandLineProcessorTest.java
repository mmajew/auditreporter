package com.egnyte.utils.auditreporter;

import com.egnyte.utils.auditreporter.formatter.CSVFormatter;
import com.egnyte.utils.auditreporter.formatter.DefaultPlainTextFormatter;
import com.egnyte.utils.auditreporter.formatter.TopNCSVFormatter;
import com.egnyte.utils.auditreporter.formatter.TopNPlainTextFormatter;
import com.egnyte.utils.auditreporter.processor.TopNProcessor;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class CommandLineProcessorTest {

    private static final String USERS_FILE = "users_file";
    private static final String FIlES_FILE = "files_file";

    @Test
    public void TopNPlainTextTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE, "--top", "3"};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        commandLineParser.parse(args);
        // Then
        assertThat(commandLineParser.getProcessors()).containsExactly(new TopNProcessor(3));
        assertThat(commandLineParser.getPresenter()).isInstanceOf(TopNPlainTextFormatter.class);
        assertThat(commandLineParser.getFilesFileName()).isEqualTo(FIlES_FILE);
        assertThat(commandLineParser.getUsersFileName()).isEqualTo(USERS_FILE);
    }

    @Test
    public void TopNCSVTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE, "--top", "3", "-c"};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        commandLineParser.parse(args);
        // Then
        assertThat(commandLineParser.getProcessors()).containsExactly(new TopNProcessor(3));
        assertThat(commandLineParser.getPresenter()).isInstanceOf(TopNCSVFormatter.class);
        assertThat(commandLineParser.getFilesFileName()).isEqualTo(FIlES_FILE);
        assertThat(commandLineParser.getUsersFileName()).isEqualTo(USERS_FILE);
    }

    @Test
    public void TopNCSVDifferentOrderTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE, "-c", "--top", "3",};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        commandLineParser.parse(args);
        // Then
        assertThat(commandLineParser.getProcessors()).containsExactly(new TopNProcessor(3));
        assertThat(commandLineParser.getPresenter()).isInstanceOf(TopNCSVFormatter.class);
        assertThat(commandLineParser.getFilesFileName()).isEqualTo(FIlES_FILE);
        assertThat(commandLineParser.getUsersFileName()).isEqualTo(USERS_FILE);
    }

    @Test
    public void ReportPlainTextTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        commandLineParser.parse(args);
        // Then
        assertThat(commandLineParser.getProcessors()).isEmpty();
        assertThat(commandLineParser.getPresenter()).isInstanceOf(DefaultPlainTextFormatter.class);
        assertThat(commandLineParser.getFilesFileName()).isEqualTo(FIlES_FILE);
        assertThat(commandLineParser.getUsersFileName()).isEqualTo(USERS_FILE);
    }

    @Test
    public void ReportCSVTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE, "-c"};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        commandLineParser.parse(args);
        // Then
        assertThat(commandLineParser.getProcessors()).isEmpty();
        assertThat(commandLineParser.getPresenter()).isInstanceOf(CSVFormatter.class);
        assertThat(commandLineParser.getFilesFileName()).isEqualTo(FIlES_FILE);
        assertThat(commandLineParser.getUsersFileName()).isEqualTo(USERS_FILE);
    }

    @Test
    public void wrongArgumentsTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, "-c"};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        Throwable throwable = catchThrowable(() -> {
            commandLineParser.parse(args);
        });
        // Then
        assertThat(throwable)
                .isInstanceOf(ParseException.class)
                .hasMessage("[USERS FILE] [FILES FILE] required");
    }

    @Test
    public void wrongOptionsTest() throws Exception {
        // Given
        String args[] = {USERS_FILE, FIlES_FILE, "-o"};
        AuditReporterCommandLineParser commandLineParser = new AuditReporterCommandLineParser();
        // When
        Throwable throwable = catchThrowable(() -> commandLineParser.parse(args));
        // Then
        assertThat(throwable)
                .isInstanceOf(ParseException.class)
                .hasMessage("Unrecognized option: -o");
    }

}