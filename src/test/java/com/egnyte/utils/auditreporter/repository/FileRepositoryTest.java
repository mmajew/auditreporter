package com.egnyte.utils.auditreporter.repository;

import com.egnyte.utils.auditreporter.AuditReporterException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static com.egnyte.utils.auditreporter.TestHelper.getFile;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class FileRepositoryTest {

    private String resourcePath;

    @Before
    public void setUp() throws Exception {
        File resourceDirectory = new File("src/test/resources");
        resourcePath = resourceDirectory.getAbsolutePath();
    }

    @Test
    public void readExistingFileTest() throws Exception {
        // Given
        FileRepository fileRepository = new FileRepository(resourcePath + "/files.csv");
        // When
        List<List<String>> files = fileRepository.readData();
        // Then
        assertThat(files).containsExactly(
                getFile("5974448e-9afd-4c9a-ac5a-9b1e84227820", 5372274L, "pic.jpg", "2"),
                getFile("fab16fa4-8251-4394-a673-c961a65eb1d2", 1638232L, "audit.xlsx", "1"),
                getFile("b4f3eecf-95aa-42a7-bffc-83a5441b7d2e", 734003200L, "movie.avi", "1"),
                getFile("675672f6-a3ff-4872-baa9-955feead534d", 570110L, "holiday.docx", "2"),
                getFile("73cadd04-c810-4b7d-9516-7b65a22a8cff", 150680L, "marketing.txt", "1")
        );
    }

    @Test
    public void readNotExistingFileTest() throws Exception {
        // Given
        FileRepository fileRepository = new FileRepository(resourcePath + "/dummy.csv");
        // When
        Throwable throwable = catchThrowable(() -> fileRepository.readData());
        // Then
        assertThat(throwable)
                .isInstanceOf(AuditReporterException.class)
                .hasMessage("Can't open file %s", resourcePath + "/dummy.csv");
    }
}