package com.egnyte.utils.auditreporter.formatter;

import com.egnyte.utils.auditreporter.AuditReporterData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.egnyte.utils.auditreporter.TestHelper.getFile;
import static com.egnyte.utils.auditreporter.TestHelper.getUser;
import static org.assertj.core.api.Assertions.assertThat;

public class DefaultCSVFormatterTest {

    private OutputFormatter outputFormatter;

    @Before
    public void setUp() throws Exception {
        outputFormatter = new DefaultCSVFormatter();
    }

    @Test
    public void formatValidDataTest() throws Exception {
        // Given
        List<List<String>> users = new ArrayList<>();
        users.add(getUser("1", "user"));
        List<List<String>> files = new ArrayList<>();
        files.add(getFile("FILE_1", 1000L, "NAME_1",  "1"));
        AuditReporterData data = new AuditReporterData(users, files);
        // When
        String header = outputFormatter.getHeader();
        String filesData = outputFormatter.formatData(data);
        // Then
        assertThat(header).isEqualToIgnoringWhitespace("Audit Report\n============");
        assertThat(filesData).isEqualToIgnoringWhitespace("user,NAME_1,1000");
    }

    @Test
    public void formatUserNotFoundDataTest() throws Exception {
        // Given
        List<List<String>> users = new ArrayList<>();
        users.add(getUser("1", "user"));
        List<List<String>> files = new ArrayList<>();
        files.add(getFile("FILE_1", 1000L, "NAME_1",  "2"));
        AuditReporterData data = new AuditReporterData(users, files);
        // When
        String header = outputFormatter.getHeader();
        String filesData = outputFormatter.formatData(data);
        // Then
        assertThat(header).isEqualToIgnoringWhitespace("Audit Report\n============");
        assertThat(filesData).isEqualToIgnoringWhitespace(",NAME_1,1000");
    }

    @Test
    public void formatEmptyFilesDataTest() throws Exception {
        // Given
        List<List<String>> users = new ArrayList<>();
        users.add(getUser("1", "user"));
        List<List<String>> files = new ArrayList<>();
        AuditReporterData data = new AuditReporterData(users, files);
        // When
        String header = outputFormatter.getHeader();
        String filesData = outputFormatter.formatData(data);
        // Then
        assertThat(header).isEqualToIgnoringWhitespace("Audit Report\n============");
        assertThat(filesData).isEqualToIgnoringWhitespace("");
    }
}