package com.egnyte.utils.auditreporter;

import java.util.ArrayList;
import java.util.List;

public class TestHelper {

    public static List<String> getFile(String fileId, long fileSize, String fileName, String ownerId) {
        List<String> file = new ArrayList<>();
        file.add(fileId);
        file.add(String.format("%d", fileSize));
        file.add(fileName);
        file.add(ownerId);
        return file;
    }

    public static List<String> getUser(String userId, String userName) {
        List<String> user = new ArrayList<>();
        user.add(userId);
        user.add(userName);
        return user;
    }
}
