AuditReporter
===============

Readme
------

AuditReporter  lists of files owned by users. Substitute JAVA_HOME with your JVM installation and you can build & run
the utility like this:

	export JAVA_HOME=/usr/lib/jvm/java-7-oracle
	./gradlew assemble
	$JAVA_HOME/bin/java -jar build/libs/auditreporter-1.0.jar src/test/resources/users.csv src/test/resources/files.csv

Usage options are as follow:
    usage: java -jar auditreporter.jar [USERS FILE] [FILES FILE]
     -c               prints results in CSV format
     -t,--top <arg>   prints n-largest files sorted by size (n should be
                      positive number)

To print usage help please execute following:

    $JAVA_HOME/bin/java -jar build/libs/auditreporter-1.0.jar


Other Considerations
--------------------

### Refactoring

I think that I failed with not over-doing the refactoring. But at least I can explain why:
  * Object-oriented design: I wanted to separate functionalities and encapsulate it in different class.
  * Unit tests: unit testing a single class with only one public "main" method is not possible. To make a code testable it
  was split up.
  * Design patterns: to use design patterns in this project refactoring was needed.

### Tests

To execute all jUnit tests please execute following:

	./gradlew test

Test report can be found in a following location:

	build/reports/tests/test/index.html

### External libraries

Following external libraries have been used:

For compilation:
    group: 'org.apache.commons', name: 'commons-text', version: '1.1'
    group: 'commons-cli', name: 'commons-cli', version: '1.4'
For tests:
    group: 'junit', name: 'junit', version: '4.12'
    group: 'org.assertj', 'name': 'assertj-core', version: '3.8.0'

All classes required in a runtime are attached to assembled jar. There is no need to change classpath for run the application.

### Changes in a specification

Following changes have been done in the specification:
  * building the AuditReader is done using the Gradle tool, not by simply javac.
  * running the AuditReader is done using executable jar, not by calling java with parameters.

If there is more differences from the specification you should treat them as bugs :)
If I forgot about something - please accept my apologies.

In case of any questions please do not hesitate to contact me.